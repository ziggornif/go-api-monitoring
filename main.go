package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go-api-monitoring/monitoring"
	"go-api-monitoring/tweet"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"net/http"
	"strconv"
	"time"
)

func prometheusHandler() gin.HandlerFunc {
	h := promhttp.Handler()

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func main() {
	gin.SetMode(gin.ReleaseMode)

	responseTimeCollector := monitoring.NewResponseTime()

	router := gin.Default()
	router.Use(cors.Default())

	dsn := "host=localhost user=monitoring password=secret dbname=monitoring-article port=5432 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	tweetService := tweet.NewTweetService(db)

	router.GET("/tweets", func(c *gin.Context) {
		start := time.Now()
		var statusCode = http.StatusOK
		tweets := tweetService.ListTweets()
		c.JSON(statusCode, tweets)
		diff := time.Now().Sub(start).Seconds() * 1000

		responseTimeCollector.Collect(c.Request.Method, c.Request.RequestURI, strconv.Itoa(statusCode), diff)
	})

	router.POST("/tweets", func(c *gin.Context) {
		start := time.Now()
		var input tweet.TweetRequest
		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			diff := time.Now().Sub(start).Seconds() * 1000
			responseTimeCollector.Collect(c.Request.Method, c.Request.RequestURI, strconv.Itoa(http.StatusBadRequest), diff)
			return
		}
		tweets := tweetService.CreateTweet(input)
		c.JSON(http.StatusOK, tweets)
		diff := time.Now().Sub(start).Seconds() * 1000
		responseTimeCollector.Collect(c.Request.Method, c.Request.RequestURI, strconv.Itoa(http.StatusOK), diff)
	})

	router.GET("/metrics", prometheusHandler())

	router.Run(":8080")
}
